FROM openjdk:8-jdk-alpine 

 
 ARG t
 ARG port
 ARG run_db
 ENV eport=$port
 ENV erun_db=$run_db
 ENV et=$t

COPY ./target/${t}/ .  
CMD java -jar ${eport} ${erun_db} ${et} 

  
